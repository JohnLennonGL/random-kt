package com.jlngls.random

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var button : Button = findViewById(R.id.button)
        button.setOnClickListener { rollDice()}
        rollDice()
    }

     fun rollDice() {

        var campoInserir = editTextTextPersonName.text.toString()
         // chamando validacao de campo
        val validaCampo = validarCampo(campoInserir)
        //validando campo
        if (validaCampo) {
            escolherNumero(campoInserir)
        }
        else {
            fotoDados.setImageResource(R.drawable.dice_1)
            resultadoID.text = "escolha um numero valido"
        }
    }

    //Verificar se o campo estar vazio ou preenchido
    fun validarCampo(campo: String): Boolean {
        var campoValidado: Boolean = true
        if (campo == null || campo.equals("")){
            campoValidado = false
        }
        return campoValidado
    }

    fun escolherNumero(campo: String) {
        var inserirNumero = campo.toInt()
        val myFirstDice = Dice(6)
        myFirstDice.diceRoll

        if (inserirNumero > 6) {
            resultadoID.text = "numero nao é valido"
            fotoResultado.setImageResource(R.drawable.proibido)

        } else if (inserirNumero != myFirstDice.diceRoll) {
            resultadoID.text = "voce perdeu"
            fotoResultado.setImageResource(R.drawable.youlose)
        } else {
            resultadoID.text = "voce ganhou"
            fotoResultado.setImageResource(R.drawable.win)
        }

        var imageView: ImageView = findViewById(R.id.fotoDados)

        // Determine qual ID de recurso drawable usar com base na jogada de dados
//        val drawableResource = when (myFirstDice.diceRoll) {
//            1 -> R.drawable.dice_1
//            2 -> R.drawable.dice_2
//            3 -> R.drawable.dice_3
//            4 -> R.drawable.dice_4
//            5 -> R.drawable.dice_5
//            else -> R.drawable.dice_6 }
//            imageView.setImageResource(drawableResource)
        if(inserirNumero > 6){
            fotoDados.setImageResource(R.drawable.alert)
        }
        else if (myFirstDice.diceRoll == 1){
            fotoDados.setImageResource(R.drawable.dice_1)
        }
        else if (myFirstDice.diceRoll == 2){
            fotoDados.setImageResource(R.drawable.dice_2)
        }
        else if (myFirstDice.diceRoll == 3){
            fotoDados.setImageResource(R.drawable.dice_3)
        }
        else if (myFirstDice.diceRoll == 4){
            fotoDados.setImageResource(R.drawable.dice_4)
        }
        else if (myFirstDice.diceRoll == 5){
            fotoDados.setImageResource(R.drawable.dice_5)
        }
        else if (myFirstDice.diceRoll == 6){
            fotoDados.setImageResource(R.drawable.dice_6)
        }
        // Atualize o ImageView com o ID de recurso drawable correto

    }

    class Dice (val numSides: Int) {

        val diceRoll = roll()

        fun roll(): Int {
            return (1..numSides).random()
        }
    }
}